using System;  
//using MonoBrick.EV3;//use this to run the example on the EV3
using MonoBrick.NXT;//use this to run the example on the NXT  
using UnityEngine;

namespace Application  
{  
    public class Program : MonoBehaviour{
        private Brick<Sensor, Sensor, Sensor, Sensor> brick;
        sbyte speed = 0;
        string sensorReadingString;
        int tachoCount;

        private void Start ()
        {
            
            try
            {  
                brick = new Brick<Sensor,Sensor,Sensor,Sensor>("usb");
                brick.Connection.Open();
                brick.Sensor1 = new TouchSensor();
                brick.Sensor1.Initialize();    
    }  
            catch(Exception e)
            {
                Debug.Log(e.Message);                    
            }     
      }

        private void Update()
        {
            //Reading Lego Sensor input for transforming a gameobject
            sensorReadingString = brick.Sensor1.ReadAsString();
            Debug.Log(sensorReadingString);
            if (sensorReadingString == "1")
            {
                transform.Translate(Vector3.forward * Time.deltaTime * 50f);
                transform.Translate(Vector3.up * Time.deltaTime, Space.World);
            }

            //Reading Lego Motor input for counting the Tacho
            tachoCount = brick.MotorA.GetTachoCount();
            Debug.Log(brick.MotorA.GetTachoCount());
            if (tachoCount > 0)
            {
                transform.Translate(Vector3.forward * Time.deltaTime);
                transform.Translate(Vector3.up * Time.deltaTime, Space.World);
            }
            else
            {
                transform.Translate(Vector3.forward * -1 * Time.deltaTime);
                transform.Translate(Vector3.up * Time.deltaTime, Space.World);
            }

        }
    }
} 

